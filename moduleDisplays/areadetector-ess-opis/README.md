This project provides [areaDetector](https://github.com/areaDetector/areaDetector) (along with [asyn](https://github.com/epics-modules/asyn) and [autosave](https://github.com/epics-modules/autosave)) OPIs in compliance with [ESS OPI Development Style Guide](https://chess.esss.lu.se/enovia/link/ESS-0341654/21308.51166.48896.47187/valid).

It is based on the work done in 2020 by @haitaoliu (Jira ticket [CSSOPI-297](https://jira.esss.lu.se/browse/CSSOPI-297)) which can be found unaltered in commit bc08845de0021627b7f10f83ebc2cf18547fb138.

50a0964d4e4857e57d797624b1c3f8c2c3846d6e is the last commit in which .adl, .edl, .opi and .ui files are available.

f7a78c512e858b826322a1b3d467dae6a93a8170 is the last commit in which the original .bob files are available.

The folder structure must be kept same as the upstream one.

If you add/update the OPIs in this repo then also create/update the submodule (under the opi folder) in their respective wrappers.

For convenience a list of wrappers/modules using this project as submodule is provided below.

- [e3-ADAndor](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADAndor)
- [e3-ADAndor3](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADAndor3)
- [ADAravis]()
- [e3-ADCore](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADCore)
- [e3-ADGenICam](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADGenICam)
- [e3-ADhama](https://gitlab.esss.lu.se/epics-modules/adhama)
- [e3-ADPointGrey](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADPointGrey)
- [e3-ADSpinnaker](https://gitlab.esss.lu.se/e3/wrappers/area/e3-ADSpinnaker)
- [aravisGigE]()
- [e3-asyn](https://gitlab.esss.lu.se/e3/wrappers/core/e3-asyn)
- [e3-autosave](https://gitlab.esss.lu.se/e3/wrappers/core/e3-autosave)
