# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Generates a main display with buttons to call specific devices displays. Read
#   information for the devices from an XML file
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Standard libraries
# -----------------------------------------------------------------------------
import xml.etree.ElementTree as ElementTree
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Specific libraries on CS-Studio
# -----------------------------------------------------------------------------
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import ActionInfos, OpenDisplayActionInfo

# -----------------------------------------------------------------------------
# Verification if the script is being called from Phoebus or from CS-Studio running on Eclipse
# -----------------------------------------------------------------------------
if 'PHOEBUS' in dir(ScriptUtil):
    from org.phoebus.pv import PVFactory
    from org.phoebus.framework.macros import Macros
else:
    from org.csstudio.display.builder.runtime.pv import PVFactory, RuntimePVFactory
    from org.csstudio.display.builder.model.macros import Macros

# -----------------------------------------------------------------------------
# Definition of constants used
# -----------------------------------------------------------------------------
# embedded
embedded_width  = 410
embedded_height = 240
x_upCornerEmbedded_start = 10
y_upCornerEmbedded_start = 70
# action_button
button_width  = 70
button_height = 30
x_upCornerButton_start = 338
y_upCornerButton_start = 270
button_x_spacer  = 410
button_y_spacer  = 240

# Dictionary used to set target
display_target = { 'REPLACE':       OpenDisplayActionInfo.Target.REPLACE,
                   'TAB':           OpenDisplayActionInfo.Target.TAB,
                   'WINDOW':        OpenDisplayActionInfo.Target.WINDOW,
                   'STANDALONE':    OpenDisplayActionInfo.Target.STANDALONE }

module_rel_paths = ["../../../moduleDisplays", ".opisControlDashboard/moduleDisplays"]

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()

# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def generatorProcedure():
    # Locate XML file relative to the display file
    display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
    directory    = os.path.dirname(display_file)
    file         = directory + "/scripts/devices.xml"
    module_path  = module_rel_paths[0] if "mainDisplays" in file else module_rel_paths[1]
    display      = widget.getDisplayModel()
    # Components settings
    embeddeds_by_row  = 4
    # - embedded
    x_upCornerEmbedded = x_upCornerEmbedded_start
    y_upCornerEmbedded = y_upCornerEmbedded_start
    # - actionButton
    x_upCornerButton = x_upCornerButton_start
    y_upCornerButton = y_upCornerButton_start

    # Parse XML
    # Actual content of the XML file would of course depend on what's needed to describe one device.
    xml = ElementTree.parse(file).getroot()
    instance_number = 0
    for device in xml.iter("device"):
        device_settings = dict()
        device_macros_dict = dict()
        device_macros_obj  = Macros()
        for element in device:
            # Debug purposes...
            # logger.info("element.tag: %s and text: %s" % (str(element.tag), str(element.text)))
            if str(element.tag) == "macros":
                for macro in element:
                    # Debug purposes...
                    # logger.info("macro.tag: %s; macro.text: %s" % (str(macro.tag), str(macro.text)))
                    # - for embedded
                    device_macros_dict[macro.tag] = macro.text
                    # - for actionButton
                    device_macros_obj.add(macro.tag, macro.text)
            else:
                device_settings[element.tag] = element.text
        # logger.info("macros: %s" % str(device_macros))
        # ---------------------------------------------------------------------
        # Calculating new X and Y coordinates...
        # ---------------------------------------------------------------------
        # - embedded
        x_pos_embedded = x_upCornerEmbedded + (instance_number * embedded_width)
        y_pos_embedded = y_upCornerEmbedded
        # - actionButton
        x_pos_button = x_upCornerButton + (instance_number * button_x_spacer)
        y_pos_button = y_upCornerButton
        # ---------------------------------------------------------------------
        # Creating a new instance of an EmbeddedDisplay
        # ---------------------------------------------------------------------
        embeddedInstance = createEmbeddedInstance(x_pos_embedded, y_pos_embedded, module_path, device_macros_dict)
        # Adding the created EmbeddedDisplay to the Display
        display.runtimeChildren().addChild(embeddedInstance)
        # ---------------------------------------------------------------------
        # Creating a new instance of an ActionButton
        # ---------------------------------------------------------------------
        buttonInstance = createButtonInstance(x_pos_button, y_pos_button, module_path, device_settings, device_macros_obj)
        # Adding the created ActionButton to the Display
        display.runtimeChildren().addChild(buttonInstance)
        # Updating control variables used to position the buttons on screen
        instance_number += 1
        if instance_number == embeddeds_by_row:
            # reset count and increase X position (inclue a new column in practice)
            instance_number = 0
            # - embedded
            x_upCornerEmbedded  =  x_upCornerEmbedded_start
            y_upCornerEmbedded  += embedded_height + 10
            # - actionButton
            x_upCornerButton    =  x_upCornerButton_start
            y_upCornerButton    += button_y_spacer + 10
        else:
            # - embedded
            x_upCornerEmbedded  += 10
            # - actionButton
            x_upCornerButton    += 10

# -----------------------------------------------------------------------------
# Create display:
# -----------------------------------------------------------------------------
# For each 'device', add one embedded display which then links to the correspondent
# .bob display with the macros of the desired device.
def createEmbeddedInstance(x, y, module_path, macros_dict):
    # Creating an instance of an openDisplayActionInfo using necessary specific objects, and add it to a list of ActionInfos
    display_file = ('%s/%s/bob/%s.bob' % (module_path, "iocStats", "iocStats"))
    # Creating an instance of an ActionButton
    embedded = WidgetFactory.getInstance().getWidgetDescriptor("embedded").createWidget();
    embedded.setPropertyValue("x", x)
    embedded.setPropertyValue("y", y)
    embedded.setPropertyValue("width", embedded_width)
    embedded.setPropertyValue("height", embedded_height)
    # ActionInfos list is passed here...
    for macro, value in macros_dict.items():
        embedded.getPropertyValue("macros").add(macro, value)
    embedded.setPropertyValue("file", display_file)
    return embedded


# And an action button to open the display according to device type...
def createButtonInstance(x_pos_button, y_pos_button, module_path, settings, macros_obj):
    # Inside embedded area the action button will always be in the same position
    # Creating an instance of an openDisplayActionInfo using necessary specific objects, and add it to a list of ActionInfos
    if (settings.get('display')):
        display_file = ('%s/%s/%s' % (module_path, settings.get('type'), settings.get('display')))
    else:
        display_file = ('%s/%s/bob/%s.bob' % (module_path, settings.get('type'), settings.get('type').split('/')[-1]))
    open_display_action = OpenDisplayActionInfo(settings.get('type'), display_file, macros_obj, display_target.get(settings.get('target')))
    action_infos = ActionInfos([open_display_action])
    # Creating an instance of an ActionButton
    action_button = WidgetFactory.getInstance().getWidgetDescriptor("action_button").createWidget();
    action_button.setPropertyValue("text", "Launch")
    action_button.setPropertyValue("x", x_pos_button)
    action_button.setPropertyValue("y", y_pos_button)
    action_button.setPropertyValue("width", button_width)
    action_button.setPropertyValue("height", button_height)
    # ActionInfos list is passed here...
    action_button.setPropertyValue("actions", action_infos)
    return action_button

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
generatorProcedure()
